const test = require('tape');
const deepstream = require('collab-fork-deepstream.io-client-js');
const dsSub = require('../').dsSub;
const R = require('ramda');
const M = require('@most/core');
const newDefaultScheduler = require('@most/scheduler').newDefaultScheduler;

test('subscribe', async t => {
  const client = deepstream('ws://...');
  client.login({
    id: '',
    password: '',
  });
  const clientSub = dsSub(client);
  const msg$ = clientSub('data/channel');
  const sum$ = M.map(
    R.compose(
      R.sum,
      R.prop('pl'),
      // JSON.parse,
      R.tap(console.log),
    ),
    msg$,
  );
  const test$ = M.startWith(JSON.stringify({ pl: [2, 3] }), sum$);
  // const sum$ = M.map(R.sum, msg$);
  M.runEffects(M.tap(console.log, test$), newDefaultScheduler());
});
