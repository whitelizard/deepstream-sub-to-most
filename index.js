const currentTime = require('@most/scheduler').currentTime;

function tryEvent(t, x, sink) {
  try {
    sink.event(t, x);
  } catch (e) {
    sink.error(t, e);
  }
}

class DsSub {
  constructor(dsClient, channel) {
    this.dsClient = dsClient;
    this.channel = channel;
  }

  run(sink, scheduler) {
    const send = msg => tryEvent(currentTime(scheduler), msg, sink);
    const dispose = () => this.dsClient.event.unsubscribe(this.channel, send);

    this.dsClient.event.subscribe(this.channel, send);

    return { dispose };
  }
}

module.exports = { dsSub: dsClient => channel => new DsSub(dsClient, channel) };
